<?php 
	interface Tables{
		
		/*@ get name table*/
		public function getNameTable();
		
		/*@ get list values of the table*/
		public function getListValues();
		
	}
?>