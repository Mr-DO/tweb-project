<?php

	require_once "Tables.php";
	class CartellaDocumenti implements Tables{
		
		static private $TABLE_NAME = "cartella_documenti";
		private $id;
		private $id_cartella;
		private $id_documenti;
		
		public function CartellaCartella($id, $idCartella, $idDocumenti){
			$this->id = $id;
			$this->id_cartella = $idCartella;
			$this->id_documenti = $idDocumenti;
		}
		
		// all getters
		public function getId(){ return $this->id; }
		public function getIdCartella(){ return $this->id_cartella; }
		public function getIdDocumenti(){ return $this->id_documenti; }
		
		public function setId($id){ $this->id = $id; }
		public function setIdCartella($idCartella){ $this->id_cartella = $idCartella; }
		public function setIdDocumenti($idDocumenti){ $this->id_documenti = $idDocumenti; }
		
		/*@ get name table*/
	public function getNameTable(){
		return CartellaDocumenti::$TABLE_NAME;
	}
	
	/*@ get list values of the table*/
	public function getListValues(){
		return "id,".$this->getId().";id_cartella,".$this->getIdCartella().";id_documenti,".$this->getIdDocumenti();
	}
	}
?>