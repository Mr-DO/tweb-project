<?php 
	
require_once "Tables.php";
class Documenti implements Tables{
	
	static private $TABLE_NAME = "documenti";
	private $id;
	private $nome;
	private $descrizione;
	private $nomeFile;
	private $visibilita;
	private $idCartella;
	private $created_at = "";
	private $updated_at = "";
	
	public function __construct($id, $nome, $descrizione, $nomefile, $visibilita, $idCartella){
		
		$this->id = $id;
		$this->nome = $nome;
		$this->descrizione = $descrizione;
		$this->nomeFile = $nomefile;
		$this->visibilita = $visibilita;
		$this->idCartella = $idCartella;
	}
	
	// all getters
	public function getId(){ return $this->id; }
	public function getNome(){ return $this->nome; }
	public function getNomeFile(){ return $this->nomeFile; }
	public function getDescrizione(){ return $this->descrizione; }
	public function getVisibilita(){ return $this->visibilita; }
	public function getIdCartella(){ return $this->idCartella; }
	public function getCreatedDate(){ return $this->created_at; }
	public function getUpdatedDate(){ return $this->updated_at; }
	
	//All setters
	// all getters
	public function setId($id){ $this->id = $id; }
	public function setNome($nome){ $this->nome = $nome; }
	public function setNomeFile($nomeFile){ $this->nomeFile = $nomeFile; }
	public function setDescrizione($descrizione){ $this->descrizione = $descrizione; }
	public function setVisibilita($visibilita){ $this->visibilita = $visibilita; }
	public function setIdCartella($idCartella){ $this->idCartella = $idCartella; }
	public function setCreatedDate($createdDate){ $this->created_at = $createdDate; }
	public function setUpdatedDate($updatedDate){ $this->updated_at = $updatedDate; }
	
	/*@ get name table*/
	public function getNameTable(){
		return Documenti::$TABLE_NAME;
	}
	
	/*@ get list values of the table*/
	public function getListValues(){
		return "id,".$this->getId().";nome,".$this->getNome().";descrizione,".$this->getDescrizione().";nomefile,".$this->getNomeFile().";visibilita,".$this->getVisibilita().";id_cartella,".$this->getIdCartella().";created_at,".$this->getCreatedDate().";updated_at,".$this->getUpdatedDate();
	}
	
}

?>