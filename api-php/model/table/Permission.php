<?php 
	
require_once "Tables.php";
class Permission implements Tables{
	
	static private $TABLE_NAME = "permission";
	private $id;
	private $nome;
	private $value;
	private $zone;
	private $descrizione;
	
	public function __construct($id, $nome, $value, $zone, $descrizione){
		$this->id = $id;
		$this->nome = $nome;
		$this->value = $value;
		$this->zone = $zone;
		$this->descrizione = $descrizione;
	}
	
	//All getters
	public function getId(){ return $this->id; }
	public function getNome(){ return $this->nome; }
	public function getDescrizione(){ return $this->descrizione; }
	public function getZone(){ return $this->zone; }
	public function getValue(){ return $this->value; }
	
	//All setters
	public function setId($id){ $this->id = $id; }
	public function setNome($nome){ $this->nome = $nome; }
	public function setDescrizione($descrizione){ $this->descrizione = $descrizione; }
	public function setZone($zone){ $this->zone = $zone; }
	public function setValue($value){ $this->value = $value; }
	
	/*@ get name table*/
	public function getNameTable(){
		return Permission::$TABLE_NAME;
	}
	
	/*@ get list values of the table*/
	public function getListValues(){
		return "id,".$this->getId().";nome,".$this->getNome().";value,".$this->getValue()."zone,".$this->getZone().";descrizione,".$this->getDescrizione();
	}
	
}

?>