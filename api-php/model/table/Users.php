<?php 
	
require_once "Tables.php";
class Users implements Tables{
	
	static private $TABLE_NAME = "users";
	private $id;
	private $userName;
	private $nome;
	private $cognome;
	private $permission;
	private $created_at = "";
	private $updated_at = "";
	
	public function __construct($id, $userName, $nome, $cognome, $permission){
	
		$this->id = $id;
		$this->userName = $userName;
		$this->nome = $nome;
		$this->cognome = $cognome;
		$this->permission = $permission;
	}
	
	// all getters
	public function getId(){ return $this->id; }
	public function getUserName(){ return $this->userName; }
	public function getNome(){ return $this->nome; }
	public function getCognome(){ return $this->cognome; }
	public function getPermission(){ return $this->permission; }
	public function getCreatedDate(){ return $this->created_at; }
	public function getUpdatedDate(){ return $this->updated_at; }
	
	// all setters
	public function setId($id){ $this->id = $id; }
	public function setUserName($username){ $this->userName = $username; }
	public function setNome($nome){ $this->nome = $nome; }
	public function setCognome($cognome){ $this->cognome = $cognome; }
	public function setPermission($permission){ $this->permission = $permission; }
	public function setCreatedDate($createdDate){ $this->created_at = $createdDate; }
	public function setUpdatedDate($updatedDate){ $this->updated_at = $updatedDate; }
	
	/*@ get name table*/
	public function getNameTable(){
		return Users::$TABLE_NAME;
	}
	
	/*@ get list values of the table*/
	public function getListValues(){
		return "id,".$this->getId().";username,".$this->getUserName().";nome,".$this->getNome().";cognome,".$this->getCognome().";permission,".$this->getPermission().";created_at,".$this->getCreatedDate().";updated_at,".$this->getUpdatedDate();
	}
	
}

?>