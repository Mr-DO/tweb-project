<?php

	require_once "Tables.php";
	class UserArea implements Tables{
		
		static private $TABLE_NAME = "user_area";
		private $id;
		private $id_user;
		private $id_area;
		
		public function CartellaCartella($id, $idUSer, $idArea){
			$this->id = $id;
			$this->id_user = $idUser;
			$this->id_area = $idArea;
		}
		
		// all getters
		public function getId(){ return $this->id; }
		public function getIdUser(){ return $this->id_user; }
		public function getIdArea(){ return $this->id_area; }
		
		public function setId($id){ $this->id = $id; }
		public function setIdUser($idUser){ $this->id_user = $idUser; }
		public function setIdArea($idArea){ $this->id_area = $idArea; }
		
		/*@ get name table*/
	public function getNameTable(){
		return UserArea::$TABLE_NAME;
	}
	
	/*@ get list values of the table*/
	public function getListValues(){
		return "id,".$this->getId().";id_user,".$this->getIdUser().";id_area,".$this->getIdArea();
	}
	}
?>