<?php 
	
require_once "Tables.php";
class Zone implements Tables{
	
	static private $TABLE_NAME = "zone";
	private $id;
	private $nome;
	private $descrizione;
	private $created_at = "";
	private $updated_at = "";
	
	
	public function __construct($id, $nome, $descrizione){
		$this->id = $id;
		$this->nome = $nome;
		$this->descrizione = $descrizione;
	}
	
	//All getters
	public function getId(){ return $this->id; }
	public function getNome(){ return $this->nome; }
	public function getDescrizione(){ return $this->descrizione; }
	public function getCreatedDate(){  return $this->created_at; }
	public function getUpdatedDate(){ return $this->updated_at; }
		
	//All setters
	public function setId($id){ $this->id = $id; }
	public function setNome($nome){ $this->nome = $nome; }
	public function setDescrizione($descrizione){ $this->descrizione = $descrizione; }
	public function setCreatedDate($startDate){ $this->created_at = $startDate; }
	public function setUpdatedDate($endDate){ $this->updated_at = $endDate; }
	
	/*@ get name table*/
	public function getNameTable(){
		return Zone::$TABLE_NAME;
	}
	
	/*@ get list values of the table*/
	public function getListValues(){
		return "id,".$this->getId().";nome,".$this->getNome().";descrizione,".$this->getDescrizione().";created_at,".$this->getCreatedDate().";updated_at,".$this->getUpdatedDate();
	}
	
}

?>