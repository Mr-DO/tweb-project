<?php

	require_once "Tables.php";
	class CartellaCartella implements Tables{
		
		static private $TABLE_NAME = "cartella_sotto_cartella";
		private $id;
		private $id_cartella;
		private $id_sotto_cartella;
		
		public function CartellaCartella($id, $idCartella, $idSottoCartella){
			$this->id = $id;
			$this->id_cartella = $idCartella;
			$this->id_sotto_cartella = $idSottoCartella;
		}
		
		// all getters
		public function getId(){ return $this->id; }
		public function getIdCartella(){ return $this->id_cartella; }
		public function getIdSottoCartella(){ return $this->id_sotto_cartella; }
		
		public function setId($id){ $this->id = $id; }
		public function setIdCartella($idCartella){ $this->id_cartella = $idCartella; }
		public function setIdSottoCartella($idSottoCartella){ $this->id_sotto_cartella = $idSottoCartella; }
		
		/*@ get name table*/
	public function getNameTable(){
		return CartellaCartella::$TABLE_NAME;
	}
	
	/*@ get list values of the table*/
	public function getListValues(){
		return "id,".$this->getId().";id_cartella,".$this->getIdCartella().";id_sotto_cartella,".$this->getIdSottoCartella();
	}
	}
?>