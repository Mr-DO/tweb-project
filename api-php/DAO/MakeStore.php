<?php 
	require_once "DAOFactory.php";
	require_once "Store.php";
	
	class MakeStore implements Store{
		
		private $selectFactory;
		private $conn;
		static private $instanceStore = NULL;
			
		private function __construct($number){
			$this->selectFactory = DAOFactory::getDAOFactory($number);
			$this->conn = $this->selectFactory->createConnection();
		}
		
		static public function getStore($number){
			if(MakeStore::$instanceStore === NULL){
				MakeStore::$instanceStore = new MakeStore($number);
			}
			return MakeStore::$instanceStore;
		}
		
		/** Get all elements in the support storage */
		public function getAll($query){
			$results = $this->conn->query($query);
			switch($this->selectFactory->modeOfStorage()){
				case "pdo":
					return $results->fetchAll(PDO::FETCH_ASSOC);
					break;
				case "mysql":
					$array_result = array();
					while($row = $results->fetch_array(MYSQLI_ASSOC)){
						$array_result[] = $row;
					}
					return $array_result;
					break;
				default:
					return array();
					break;
			}
		}
		
		/** insert current element in the support storage */
		public function insertInto($query){
			switch($this->selectFactory->modeOfStorage()){
				case "pdo":
					try { $this->conn->exec($query); }
					catch(PDOException $e){ return FALSE; }
					return TRUE;
					break;
				case "mysql":
					return $this->conn->query($query);
					break;
				default:
					return FALSE;
					break;
			}
		}
		
		/** Delete current element in the support storage */
		public function deleteTo($query){
			$this->insertInto($query);
		}
		
		/** Update current element in the support storage */
		public function update($query){
			$this->insertInto($query);
		}
	}

?>