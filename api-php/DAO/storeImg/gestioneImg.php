<?php
	/**
	 * 
	 */
	class GestioneImg{

		private $messagError = 100;
		private $nomeFile = "";
		private $size = 0;
		private $tmpName = "";
		private $typeImg = "";
		private $isTrue = false;
		
		public function __construct($fileName){
			var_dump($fileName);
			if(isset($fileName['name']) 
			   and isset($fileName['type']) 
			   and isset($fileName['tmp_name']) 
			   and isset($fileName['error']) 
			   and isset($fileName['size'])
			  ){
				$this->messagError = $fileName['error'];
				$this->nomeFile = $fileName['name'];
				$this->size = $fileName['size'];
				$this->tmpName = $fileName['tmp_name'];
				$this->typeImg = $fileName['type'];
				$this->isTrue = true;
			}
		}

		public function valideSize($inputSize){
			if($this->size > $inputSize or $this->size <= 0)
				return false;
			else return true;
		}

		public function isError(){
			if($this->messagError != 0)
				return true;
			else return false;
		}

		public function fileName(){
			return $this->nomeFile;
		}

		public function typeFile(){
			$typefile = explode("/", $this->typeImg);
			return $typefile[1];
		}

		public function getTmpName(){
			return $this->tmpName;
		}
	}
?>