<?php
	require_once "MakeStore.php";

	class DataBaseHelper{
		
		static private $instanceDBhelper = NULL;
		private $makeStore;
		
		// All variable stattic such as query
		private function __construct($numberDb){
			$this->makeStore = MakeStore::getStore($numberDb);
		}
		
		static public function getInstance($numberDb){
			if(DataBaseHelper::$instanceDBhelper == NULL){
				DataBaseHelper::$instanceDBhelper = new DataBaseHelper($numberDb);
			}
			return DataBaseHelper::$instanceDBhelper;	
		}
		
		/** Get all elements in the support storage */
		private function getAll($nameTable){
			$query = "SELECT * FROM $nameTable";
			return $this->makeStore->getAll($query);
		}
		
		/** Get element by id in the support storage */
		private function getById($nameTable, $id){
			$query = "SELECT * FROM $nameTable WHERE id = $id";
			return $this->makeStore->getAll($query);
		}
		
		/** insert current element in the support storage */
		private function insertInto($table){
			$tableName = $table->getNameTable();
			$keyRow = "(";
			$keyValue = "(";
			foreach(explode(';', $table->getListValues()) as $key => $value){
				if(explode(',', $value)[0] !== "id"){
					$keyRow .= explode(',', $value)[0] . ", ";
					if((explode(',', $value)[0] === "created_at" or explode(',', $value)[0] === "updated_at") and explode(',', $value)[1] ===""){
						$keyValue .= "CURRENT_TIMESTAMP, ";
					}
					else{
						$keyValue .= "'" . explode(',', $value)[1] . "', ";
					}
				}
			}
			$keyRow = trim($keyRow, ' ,');
			$keyValue = rtrim($keyValue, ' ,');
			$keyRow .= ")";
			$keyValue .= ")";
			$query = "INSERT INTO $tableName $keyRow Values$keyValue";
			echo $query;
			return $this->makeStore->insertInto($query);
		}
		
		/** Delete current element in the support storage */
		private function deleteTo($nameTable, $id){
			$query = "DELETE FROM $nameTable WHERE id = $id";
			return $this->makeStore->deleteTo($query);
		}
		
		/** Update current element in the support storage */
		private function update($table, $id){
			$tableName = $table->getNameTable();
			$key_value = "";
			foreach(explode(';', $table->getListValues()) as $key => $value){
				if(explode(',', $value)[0] !== "id"){
					if((explode(',', $value)[0] === "created_at" or explode(',', $value)[0] === "updated_at") and explode(',', $value)[1] ===""){
						$key_value .= explode(',', $value)[0] . "= CURRENT_TIMESTAMP, ";
					}
					else{
						$key_value .= explode(',', $value)[0] . "='" . explode(',', $value)[1] . "', ";
					}
				}
			}
			$key_value = rtrim($key_value, ' ,');
			$query = "UPDATE $tableName " . "SET $key_value WHERE id = $id";
			echo $query;
			return $this->makeStore->insertInto($query);
		}
		
		
		// All function for get all rows in the reference table.
		public function getUsers(){ return $this->getAll("users"); }
		public function getZones(){ return $this->getAll("zone"); }
		public function getPermissions(){ return $this->getAll("permission"); }
		public function getDocuments(){ return $this->getAll("documenti"); }
		public function getFolders(){ return $this->getAll("cartella"); }
		public function getAreas(){ return $this->getAll("area"); }
		
		public function getFoldersOfFolder($id){ 
			$query = "SELECT cartella.*
			FROM cartella JOIN cartella_sotto_cartella ON cartella.id = id_sotto_cartella
			WHERE id_cartella = $id";
			return $this->makeStore->getAll($query);
		}
		public function getDocumentsOfFolder($id){ 
			$query = "SELECT * FROM documenti WHERE id_cartella = $id";
			return $this->makeStore->getAll($query);
		}
		public function getAreasOfZone($idZone){
			$query = "SELECT * FROM area WHERE id_zone = $idZone";
			return $this->makeStore->getall($query);
		}
		public function getFoldersOfArea($idArea){
			$query = "SELECT * FROM cartella WHERE id_area = $idArea AND id NOT IN (SELECT id_sotto_cartella FROM cartella_sotto_cartella)";
			return $this->makeStore->getall($query);
		}
		
		// All function get element row by id in the reference table
		public function getUser($id){ return $this->getById("users", $id); }
		public function getZone($id){ return $this->getById("zone", $id); }
		public function getPermission($id){ return $this->getById("permission", $id); }
		public function getDocument($id){ return $this->getById("documenti", $id); }
		public function getFolder($id){ return $this->getById("cartella", $id); }
		public function getArea($id){ return $this->getById("area", $id); }
		
		// All function for insert row in the reference table.
		public function insertIntoUser($table){ return $this->insertInto($table); }
		public function insertIntoZone($table){ return $this->insertInto($table); }
		public function insertIntoPermission($table){ return $this->insertInto($table); }
		public function insertIntoDocument($table){ return $this->insertInto($table); }
		public function insertIntoFolder($table){ return $this->insertInto($table); }
		public function insertIntoArea($table){ return $this->insertInto($table); }
		public function insertIntoFolderInFolder($table){ return $this->insertInto($table); }
		public function insertIntoDocumentInFolder($table){ return $this->insertInto($table); }
		
		// All function for update an row table in the reference table
		public function deleteUser($id){ return $this->deleteTo("users", $id); }
		public function deleteZone($id){ return $this->deleteTo("zone", $id); }
		public function deletePermission($id){ return $this->deleteTo("permission", $id); }
		public function deleteDocument($id){ return $this->deleteTo("documenti", $id); }
		public function deleteFolder($id){ return $this->deleteTo("cartella", $id); }
		public function deleteArea($id){ return $this->deleteTo("area", $id); }
		
		// All function for update an row table in the reference table
		public function updateUser($table, $id){ return $this->update($table, $id); }
		public function updateZone($table, $id){ return $this->update($table, $id); }
		public function updatePermission($table, $id){ return $this->update($table, $id); }
		public function updateDocument($table, $id){ return $this->update($table, $id); }
		public function updateFolder($table, $id){ return $this->update($table, $id); }
		public function updateArea($table, $id){ return $this->update($table, $id); }
	}

?>