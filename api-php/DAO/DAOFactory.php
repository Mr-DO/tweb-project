<?php 
	require_once "PdODAOFactory.php";
	require_once "MySQLDAOFactory.php";

	abstract class DAOFactory{

		/* For get fatory mysql connection*/
		protected static $MYSQL = 0;
		/* For get fatory PDO connectio*/
		protected static $PDO   = 1;
		/* Selected factory connection*/
		protected static $SELECT;

		/* abstract method for created connection*/
		abstract public function createConnection();

		/**
		 * Method DAOFatory
		 * @param database
		 * @argoment for choose database
		 * return the corrispondante database
		 */

		static public function getDAOFactory($numDb){
			DAOFactory::$SELECT = $numDb;
			switch($numDb){
				case DAOFactory::$MYSQL:
					return new MySQLDAOFactory();
				case DAOFactory::$PDO:
					return new PdODAOFactory();
				default :
					return null;
			}
		}
		
		/**
		 * Method DAOFatory
		 * @No take paramater
		 * return the name of connection
		 */
		public function modeOfStorage(){
			switch(PdODAOFactory::$SELECT){
				case PdODAOFactory::$MYSQL:
					return "mysql";
				case PdODAOFactory::$PDO:
					return "pdo";
				default:
					return "";
			}
		}
	}
?>