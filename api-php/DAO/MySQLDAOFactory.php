<?php 
	require_once "DAOFactory.php";

	class MySQLDAOFactory extends DAOFactory{
		
		/**
		*@ Server paramaters
		*/
		static $SERVER = array(
			'host' => 'localhost',
			'database' => 'diaconia-dev',
			'username' => 'root',
			'password' => '');

		/**
		 * Method for create connection
		 * @return object connection
		 */

		public function createConnection(){
			$server = MySQLDAOFactory::$SERVER;
			$connMysql = new mysqli($server['host'], $server['username'], $server['password'], $server['database']);
			if ($connMysql->connect_error) {
    			die("Connection failed: " . $connMysql->connect_error);
				return NULL;
			} 
				return $connMysql;
		}
		
	}
?>